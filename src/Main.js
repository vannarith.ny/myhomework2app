import React, {Component} from 'react';
import {Text, View, StyleSheet} from 'react-native';
import Card from './component/Card';

type Props = {};
export default class Main extends Component<Props> {
    state = {
        person1: {
            imgurl: require('./images/img1.jpg'),
            name : 'Vannarith Ny',
            age : '29',
            gander: 'M',
            dob: '1992'
        },
        person2: {
            imgurl: require('./images/img2.jpg'),
            name : 'Kok Dara',
            age : '28',
            gander: 'M',
            dob: '1993'
        },
        person3: {
            imgurl: require('./images/img3.jpg'),
            name : 'Sok Lisa',
            age : '21',
            gander: 'F',
            dob: '1998'
        },
        person4: {
            imgurl: require('./images/img4.jpg'),
            name : 'Doung Sreyleak',
            age : '22',
            gander: 'F',
            dob: '1997'
        }
    }
  render() {
    const { containerStyle, labelText, headerStyle } = styles;
    return (
      <View style={containerStyle}>
      
        <View style={headerStyle}>
            <Text style={labelText}>Homework : Card List </Text>
        </View>

        <Card person={this.state.person1} />
        <Card person={this.state.person2} />
        <Card person={this.state.person3} />
        <Card person={this.state.person4} />

      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerStyle: {
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: '#DDDDDD',
    flex: 1,
    fontFamily: 'Helvetica Neue',
  },
  headerStyle:{
      backgroundColor: '#39A1FF',
  },
  containerRow:{
    flexDirection: 'row',
  },
  labelText: {
      fontSize: 20,
      paddingTop: 10,
      paddingLeft: 10,
      paddingBottom: 10,
      color: '#FFF',
  },
  flatlistStyle:{
      width: '50%',
  }
})


