import React from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';

const Card = (props) => {

    const { imgurl, name, age, gander, dob } = props.person;
    const { cardRow, imgBlock, txtBlock, imgStyle } = styles;

    return (
        <View style={cardRow}>
            <View style={imgBlock}>
                <Image 
                    source={imgurl} 
                    style={imgStyle}
                    resizeMode="cover"
                    />
            </View>
            <View style={txtBlock}>
                <Text>Name : {name}</Text>
                <Text>Age : {age}</Text>
                <Text>Gender : {gander}</Text>
                <Text>Date of Birth : {dob}</Text>
            </View>
        </View>
        
    )
}

const styles = StyleSheet.create({
    cardRow:{
      flexDirection: 'row',
      width: '90%',
      marginLeft: '5%',
    //   marginBottom: '5%',
      marginTop:20,
      backgroundColor: '#FFF',
      alignItems: 'center',
      padding: 20,
      borderRadius: 10,
    },
    imgBlock:{
        width:'25%',
        marginRight:'5%',
        backgroundColor: '#ccc',
        borderWidth: 1,
        borderColor: '#ddd',
        height: 80,
    },
    txtBlock:{
        width: '70%',
        height: 80,
    },
    imgStyle:{
        width: '100%',
        height: '100%',
    }
  })

export default Card